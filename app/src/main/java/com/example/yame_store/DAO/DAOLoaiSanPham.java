package com.example.yame_store.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.yame_store.Entity.LoaiSanPham;

import java.util.List;

@Dao
public interface DAOLoaiSanPham {
    @Query("Select * from loaisanpham_db")
    List <LoaiSanPham> loaiSanPham_LIST();
    @Insert
    void InsertLoaiSanpham(LoaiSanPham loaiSanPham);
    @Delete
    void DeleteLoaiSanpham(LoaiSanPham loaiSanPham);
    @Update
    void UpdateLoaiSanpham(LoaiSanPham loaiSanPham);

}
