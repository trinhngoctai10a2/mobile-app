package com.example.yame_store.Entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.DecimalFormat;

@Entity(tableName = "donhang_db")
public class DonHang {
    @PrimaryKey(autoGenerate = true)
    int Id;
    String tenMatHang;
    int soLuong;
    Double thanhTien;
    byte [] anhDonHang;


    public DonHang(String tenMatHang, int soLuong, Double thanhTien, byte[] anhDonHang) {
        this.tenMatHang = tenMatHang;
        this.soLuong = soLuong;
        this.thanhTien = thanhTien;
        this.anhDonHang = anhDonHang;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTenMatHang() {
        return tenMatHang;
    }

    public void setTenMatHang(String tenMatHang) {
        this.tenMatHang = tenMatHang;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public Double getThanhTien() {
        return thanhTien;
    }

    public String getThanhTienFormat() {
        return new DecimalFormat("###,###").format(thanhTien);
    }

    public void setThanhTien(Double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public byte[] getAnhDonHang() {
        return anhDonHang;
    }

    public void setAnhDonHang(byte[] anhDonHang) {
        this.anhDonHang = anhDonHang;
    }
}
