package com.example.yame_store.Activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.yame_store.Fragment.HoaDonFragment;
import com.example.yame_store.Fragment.SanPhamFragment;
import com.example.yame_store.Fragment.TaiKhoanFragment;
import com.example.yame_store.Fragment.ThongKeFragment;
import com.example.yame_store.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    DrawerLayout drawerLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_chu);

        bottomNavigationView = findViewById(R.id.nav_bottom_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.layout_big, new SanPhamFragment()).commit();

    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //click bottom menu
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.sanPhamFragment:
                            selectedFragment = new SanPhamFragment();
                            break;
                        case R.id.hoaDonFragment:
                            selectedFragment = new HoaDonFragment();
                            break;
                        case R.id.thongKeFragment:
                            selectedFragment = new ThongKeFragment();
                            break;
                        case R.id.taiKhoanFragment:
                            selectedFragment = new TaiKhoanFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.right_to_left, R.anim.exit_rtl, R.anim.left_to_right, R.anim.exit_ltr).replace(R.id.layout_big, selectedFragment).commit();
                    return true;
                }
            };
}
