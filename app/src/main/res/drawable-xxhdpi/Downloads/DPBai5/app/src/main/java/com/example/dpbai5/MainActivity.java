package com.example.dpbai5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import com.google.android.material.circularreveal.CircularRevealWidget;

public class MainActivity extends AppCompatActivity {

    EditText etGioBD, etPhutBD, etGiayBD, etGioKT, etPhutKT, etGiayKT;
    RadioButton rbT2T6, rbCN, rbTT, rb171;
    Button btnTinhTien;
    TextView tvKQ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etGioBD = (EditText) findViewById(R.id.etGioBD);
        etPhutBD = (EditText) findViewById(R.id.etPhutBD);
        etGiayBD = (EditText) findViewById(R.id.etGiayBD);
        etGioKT = (EditText) findViewById(R.id.etGioKT);
        etPhutKT = (EditText) findViewById(R.id.etPhutKT);
        etGiayKT = (EditText) findViewById(R.id.etGiayKT);
        rbT2T6 = (RadioButton) findViewById(R.id.rbT2T6);
        rbCN = (RadioButton) findViewById(R.id.rbCN);
        rbTT = (RadioButton) findViewById(R.id.rbTT);
        rb171 = (RadioButton) findViewById(R.id.rb171);
        btnTinhTien = (Button) findViewById(R.id.btnTinhTien);
        tvKQ = (TextView) findViewById(R.id.tvKQ);

        btnTinhTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String timeBD = etGioBD.getText().toString()+":"+etPhutBD.getText().toString()+":"+etGiayBD.getText().toString();
                String timeKT = etGioKT.getText().toString()+":"+etPhutKT.getText().toString()+":"+etGiayKT.getText().toString();

                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                Date date1 = null;
                try {
                    date1 = format.parse(timeBD);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date date2 = null;
                try {
                    date2 = format.parse(timeKT);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long thoigian = date2.getTime() - date1.getTime();

                int lock = (int) thoigian/6000;
                if (thoigian%6000 != 0)
                    lock++;
                double dongia=0;
                if (rbTT.isChecked())
                    if (rbT2T6.isChecked())
                        dongia=89.09;
                    else
                        dongia=80.18;
                else
                if (rbT2T6.isChecked())
                    dongia=75.73;
                else
                    dongia=71.94;
                DecimalFormat decimalFormat = new DecimalFormat("#.##");
                tvKQ.setText("Tổng tiền cần thanh toán : "+decimalFormat.format(lock*dongia));

            }
        });
    }
}